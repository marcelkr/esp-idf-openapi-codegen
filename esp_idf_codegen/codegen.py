from datetime import datetime
from typing import List, Dict, Optional
from pathlib import Path
import sys
import inflection
import chevron
import yaml
import esp_idf_codegen.utils
from esp_idf_codegen.model import *
from esp_idf_codegen.cjsonresolver import *
from esp_idf_codegen.route import *
from esp_idf_codegen.group import *
from esp_idf_codegen.apidefinition import ApiDefinition


class ApiInfo:
    def __init__(self, info_dict: Dict):
        self.title = info_dict['title']
        self.version = info_dict['version']
        self.generated_at = datetime.now().isoformat(' ')

    def to_dict(self):
        return {
            'api_title': self.title,
            'api_version': self.version,
            'api_generated_at': self.generated_at
        }


class Schema:
    def __init__(self, path: str, model: Model):
        self.path = path
        self.model = model

    def __repr__(self):
        return f'{self.path}: {self.model.name}'


class Codegenerator:
    def __init__(self, settingsfile: str = None):
        self.groups: List[Group] = []
        self.schemas: List[Schema] = []
        self._settings = {}
        self._num_handlers = 0

        api = ApiDefinition.get_instance()
        self.api_definition = api.get_api()
        self.api_info = api.get_info()

        self._load_settings(settingsfile)
        self._process_paths()
        self._process_schemas()

    def _load_settings(self, settingsfile: str = None):
        settings_from_file = {}
        settings_default = {
            'timestamp': True,
        }

        if settingsfile:
            settings_content = open(settingsfile)
            settings_from_file = yaml.load(settings_content, Loader=yaml.SafeLoader)
            settings_default.update(settings_from_file)

        self._settings = settings_default

    def _add_route(self, path: str, route: Route) -> None:
        """Add a route to the correct group based on the singularized root of the path
        E.g. `/pets/add` will end up in the `pet` group
        """
        path_fragments = path[1:].split('/')
        path_base_name = inflection.singularize(path_fragments[0])

        for group in self.groups:
            if group.name == path_base_name:
                group.append_route(route)
                self._num_handlers = self._num_handlers + 1;
                break
        else:
            new_group = Group(path_base_name)
            new_group.append_route(route)
            self._num_handlers = self._num_handlers + 1;
            self.groups.append(new_group)

    def _process_paths(self) -> None:
        for path, methods in self.api_definition['paths'].items():
            if '{' in path:
                print(f"Skipping route '{path}' containing a parameter. Route-Parametes are not supported.")
                # raise PathFormatNotSupportedError("TODO")
                continue
            for method in methods:
                supported_methods = ('get', 'post', 'head', 'delete', 'put', 'patch')
                if method in supported_methods:
                    try:
                        self._add_route(path, Route(path, method, self.api_definition['paths'][path][method]))
                    except EspIdfCodegenException as e:
                        print(f'Error handling path {path}: {e}')
                        sys.exit(2)
                else:
                    print(f"Skipping route '{path}' with method '{method}'. Supported methods: {supported_methods}.")
                    continue

    def _process_schemas(self) -> None:
        """Generate models for the schmas found in `/components/schemas`"""
        for name, definition in self.api_definition['components']['schemas'].items():
            try:
                self.schemas.append(Schema(f'#/components/schemas/{name}', BaseModel.factory(name, definition)))
            except EspIdfCodegenException as ex:
                print(f'Error handling schema `{name}`: {ex}')
                sys.exit(2)

    def _print_models(self, indent: int = 0) -> None:
        print()
        print('Models from /components/schemas:')
        for schema in self.schemas:
            print(f'{indent * " "}- {schema.model.name}')
            self._print_model_properties(schema.model, indent * 2)

        print()
        print('Models from request bodies:')
        for group in self.groups:
            for route in group.routes:
                if route.request_body and route.request_body.model:
                    model = route.request_body.model.name if isinstance(route.request_body.model, Model) else route.request_body.model.path
                    print(f'{indent * " "}- {model}')
                    if isinstance(route.request_body.model, Model):
                        self._print_model_properties(route.request_body.model, indent * 2)

    @staticmethod
    def _print_model_properties(model: Model, indent: int = 0) -> None:
        for prop in model.properties:
            print(f'{indent * " "}- {prop.name}')

    def print_routes(self) -> None:
        print('Routes:')
        for group in self.groups:
            print(f'  Group: {group.name}')

            for route in group.routes:
                print(f'    {route.method.upper():5} {route.path}')

                if route.request_body:
                    model = route.request_body.model.name if isinstance(route.request_body.model, Model) else route.request_body.model.path
                    print(f'      Request with body. Model: {model}')
                    # self._print_model_properties(route.request_body.model, 8)

                for response in route.responses:
                    print(f'      Response {response.code} without content')

        self._print_models(2)

    def _generate_models(self, overwrite: tuple, out_dir: str = 'generated', used_models_only: bool = False) -> None:
        used_schemas = []

        for group in self.groups:
            for route in group.routes:
                if route.request_body and route.request_body.model:
                    if isinstance(route.request_body.model, ModelReference):
                        # Resolve the model but don't generate a model file.
                        # The model file will be generated below based on the value of `used_models_only`
                        model_ref = route.request_body.model
                        for schema in self.schemas:
                            if model_ref.path == schema.path:
                                route.request_body.model = schema.model
                                used_schemas.append(schema)
                    else:
                        data = route.request_body.model.to_dict()
                        data.update(self.api_info.to_dict())
                        data.update(self._settings)
                        filepath = f'{out_dir}/models/{route.request_body.model.header_filename}'
                        generate_file(sys.path[0] + '/templates/model.mustache', filepath, data, overwrite[0])

        schemas_to_generate = used_schemas if used_models_only else self.schemas
        for schema in schemas_to_generate:
            data = schema.model.to_dict()
            data.update(self.api_info.to_dict())
            data.update(self._settings)
            filepath = f'{out_dir}/models/{schema.model.header_filename}'
            generate_file(sys.path[0] + '/templates/model.mustache', filepath, data, overwrite[0])

    def _generate_cmakelists(self, overwrite: tuple, out_dir: str = 'generated') -> None:
        pass

    def _generate_main_file(self, overwrite: tuple, out_dir: str = 'generated') -> None:
        data = {'groups': [group for group in self.groups], 'num_handlers': self._num_handlers}
        data.update(self.api_info.to_dict())
        data.update(self._settings)

        include_path = Path(f'{out_dir}/include')
        if not include_path.exists():
            include_path.mkdir()

        generate_file(f'{sys.path[0]}/templates/api.mustache', f'{out_dir}/api.c', data, overwrite[0])
        generate_file(f'{sys.path[0]}/templates/api_h.mustache', f'{out_dir}/include/api.h', data, overwrite[0])

    def _generate_handlers(self, overwrite: tuple, out_dir: str = 'generated') -> None:
        for group in self.groups:
            group_dict = group.to_dict()
            group_dict.update(self.api_info.to_dict())
            group_dict.update(self._settings)
            generate_file(f'{sys.path[0]}/templates/group_handler.mustache', f'{out_dir}/handlers/{group.source_filename}', group_dict,
                          overwrite[0])
            generate_file(f'{sys.path[0]}/templates/group_handler_h.mustache', f'{out_dir}/handlers/{group.header_filename}', group_dict,
                          overwrite[0])
            generate_file(f'{sys.path[0]}/templates/user_callback.mustache', f'{out_dir}/callbacks/{group.user_source_filename}', group_dict,
                          overwrite[1])
            generate_file(f'{sys.path[0]}/templates/user_callback_h.mustache', f'{out_dir}/callbacks/{group.user_header_filename}', group_dict,
                          overwrite[1])

    def _generate_utils(self, overwrite: tuple, out_dir: str = 'generated') -> None:
        data = self.api_info.to_dict()
        generate_file(f'{sys.path[0]}/templates/api_utils.mustache', f'{out_dir}/api_utils.c', data, overwrite[0])
        generate_file(f'{sys.path[0]}/templates/api_utils_h.mustache', f'{out_dir}/api_utils.h', data, overwrite[0])

    def generate(self, out_dir: str = 'generated', overwrite: int = 0, only_used_models: bool = False) -> None:
        if overwrite == 1:
            overwrite_tuple = (True, False)
        elif overwrite == 2:
            overwrite_tuple = (True, True)
        else:
            overwrite_tuple = (False, False)

        self._generate_models(overwrite_tuple, out_dir, only_used_models)
        self._generate_handlers(overwrite_tuple, out_dir)
        self._generate_main_file(overwrite_tuple, out_dir)
        self._generate_cmakelists(overwrite_tuple, out_dir)
        self._generate_utils(overwrite_tuple, out_dir)


def generate_file(template_path: str, output_path: str, content: Dict, overwrite: bool) -> None:
    with Path(template_path).open() as template_file:
        outfile = Path(output_path)

        if not outfile.parent.exists():
            outfile.parent.mkdir()

        if not overwrite and outfile.exists():
            print(f'File `{outfile}` already exists. Skipping generation.')
        else:
            with open(outfile, 'w') as source_file:
                source_file.write(chevron.render(template=template_file.read(),
                                                 partials_path=f'{sys.path[0]}/templates/partials',
                                                 data=content))