from typing import Dict
from esp_idf_codegen.model import *

class Route:
    def __init__(self, path: str, method: str, path_definition: Dict):
        self.path = path
        self.method = method
        self.summary = path_definition['summary'] if 'summary' in path_definition else None
        self.tags = path_definition['tags'] if 'tags' in path_definition else None
        self.handler_function_name = self.path[1:].replace('/', '_') + '_' + self.method
        self.user_callback_function_name = self.path[1:].replace('/', '_') + '_' + self.method + '_user'
        self.uri_struct_name = "uri_" + self.handler_function_name
        # TODO: distinguish between parameters in path, header, ...
        self.parameters = path_definition['parameters'] if 'parameters' in path_definition else None
        self.responses = [Response(path, code, definition) for code,
                          definition in path_definition['responses'].items()]
        self.request_body = RequestBody(path, method, path_definition['requestBody']) if 'requestBody' in path_definition else None

    def to_dict(self) -> Dict:
        return {
            'path': self.path,
            'method': self.method.upper(),
            'esp32_method': f'HTTP_{self.method.upper()}',
            'summary': self.summary,
            'tags': self.tags,
            'uri_struct_name': self.uri_struct_name,
            'handler_function_name': self.handler_function_name,
            'user_callback_function_name': self.user_callback_function_name,
            'responses?': True if self.responses else False,
            'responses': [response.to_dict() for response in self.responses],
            'requestBody': self.request_body.to_dict() if self.request_body else None,
            'parameters?': True if self.parameters else False,
            'parameters': self.parameters if self.parameters else None
        }


class Response:
    def __init__(self, path: str, code: int, response_definition: Dict):
        self.code: int = code
        self.description: str = response_definition['description']

    def to_dict(self) -> Dict:
        return {
            'code': self.code,
            'description': self.description
        }


class RequestBody:
    def __init__(self, path: str, method: str, request_definition: Dict):
        self.model: Optional[BaseModel] = None

        if '$ref' in request_definition:
            # TODO: implement
            # api = ApiDefinition.get_instance()
            # model_schema = get_dict_from_reference(api.get_api(), request_definition['$ref'])
            # self.model = BaseModel.factory(model_name, model_schema)
            raise UnsupportedApiSpecificationError('$ref as a direct child of requestBody is not supported.')
        if 'content' in request_definition:
            path_segments = path[1:].split('/')
            singularized_segments = [inflection.singularize(x) for x in path_segments]
            singularized_segments.append(method)
            model_name = ('_').join(singularized_segments)

            self.model = BaseModel.factory(model_name, request_definition['content']['application/json']['schema'])

    def to_dict(self) -> Dict:
        return {
            'model': self.model.to_dict() if self.model is not None else {}
        }