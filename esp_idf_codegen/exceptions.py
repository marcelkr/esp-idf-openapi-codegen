class EspIdfCodegenException(Exception):
    pass


class UnsupportedApiSpecificationError(EspIdfCodegenException):
    pass


class MissingPropertyError(EspIdfCodegenException):
    pass


class PathFormatNotSupportedError(EspIdfCodegenException):
    pass


class DatatypeError(EspIdfCodegenException):
    pass
