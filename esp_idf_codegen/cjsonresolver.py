from typing import Dict
from esp_idf_codegen.exceptions import DatatypeError

class CjsonResolver:
    def __init__(self, datatype: str, dataformat: str):
        self.details: Dict

        prop_lut = [
            {'type': 'string', 'format': None, 'c_type': 'char*', 'validator_function': 'cJSON_IsString',
             'struct_element': 'valuestring', 'nullvalue': 'NULL'},
            {'type': 'boolean', 'format': None, 'c_type': 'bool', 'validator_function': 'cJSON_IsBool',
             'struct_element': 'valueint', 'nullvalue': 'false'},
            {'type': 'number', 'format': 'float', 'c_type': 'float', 'validator_function': 'cJSON_IsNumber',
             'struct_element': 'valuedouble', 'nullvalue': 0.0},
            {'type': 'number', 'format': 'double', 'c_type': 'double', 'validator_function': 'cJSON_IsNumber',
             'struct_element': 'valuedouble', 'nullvalue': 0.0},
            {'type': 'integer', 'format': 'int8', 'c_type': 'int8_t', 'validator_function': 'cJSON_IsNumber',
             'struct_element': 'valueint', 'nullvalue': 0},
            {'type': 'integer', 'format': 'int16', 'c_type': 'int16_t', 'validator_function': 'cJSON_IsNumber',
             'struct_element': 'valueint', 'nullvalue': 0},
            {'type': 'integer', 'format': 'int32', 'c_type': 'int32_t', 'validator_function': 'cJSON_IsNumber',
             'struct_element': 'valueint', 'nullvalue': 0},
            {'type': 'integer', 'format': 'int64', 'c_type': 'int64_t', 'validator_function': 'cJSON_IsNumber',
             'struct_element': 'valueint', 'nullvalue': 0},
            {'type': 'integer', 'format': 'uint8', 'c_type': 'uint8_t', 'validator_function': 'cJSON_IsNumber',
             'struct_element': 'valueint', 'nullvalue': 0},
            {'type': 'integer', 'format': 'uint16', 'c_type': 'uint16_t', 'validator_function': 'cJSON_IsNumber',
             'struct_element': 'valueint', 'nullvalue': 0},
            {'type': 'integer', 'format': 'uint32', 'c_type': 'uint32_t', 'validator_function': 'cJSON_IsNumber',
             'struct_element': 'valueint', 'nullvalue': 0},
            {'type': 'integer', 'format': 'uint64', 'c_type': 'uint64_t', 'validator_function': 'cJSON_IsNumber',
             'struct_element': 'valueint', 'nullvalue': 0},
        ]

        for prop in prop_lut:
            if datatype == prop['type'] and (dataformat == prop['format'] or not prop['format']):
                self.details = prop
                break
        else:
            raise DatatypeError(f'A combination of type={datatype} and format={dataformat} is currently not supported')

    def get_c_type(self) -> str:
        return self.details['c_type']

    def get_cjson_validator_function(self) -> str:
        return self.details['validator_function']

    def get_cjson_struct_element(self) -> str:
        return self.details['struct_element']

    def get_nullvalue(self):
        return self.details['nullvalue']