from esp_idf_codegen.route import *

class Group:
    def __init__(self, name: str):
        self.name: str = name
        self.header_filename: str = f'api_{self.name}.h'
        self.source_filename: str = f'api_{self.name}.c'
        self.user_header_filename: str = f'api_{self.name}_user.h'
        self.user_source_filename: str = f'api_{self.name}_user.c'
        self.init_function_name: str = f'api_{self.name}_init'
        self.routes: List[Route] = []

    def append_route(self, route: Route) -> None:
        self.routes.append(route)

    def to_dict(self) -> Dict:
        route_dict = [route.to_dict() for route in self.routes]
        return {
            'init_function_name': self.init_function_name,
            'header_filename': self.header_filename,
            'source_filename': self.source_filename,
            'user_header_filename': self.user_header_filename,
            'user_source_filename': self.user_source_filename,
            'routes': route_dict
        }
