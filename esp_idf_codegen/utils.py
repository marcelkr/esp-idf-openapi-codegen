from typing import List, Dict, Optional

class InvalidReferenceError(Exception):
    pass

class MalformedReferenceError(Exception):
    pass

def get_resolved_schema(partial: Dict, the_dict: Dict) -> Dict:
    new_schema = {}
    for key, value in partial.items():
        if isinstance(value, dict):
            value = get_resolved_schema(value, the_dict)

        if key == '$ref':
            referenced_dict = get_dict_from_reference(the_dict, value)
            new_schema.update(referenced_dict)
        else:
            new_schema[key] = value

    for key, value in new_schema.items():
        if key == '$ref':
            new_schema = get_resolved_schema(new_schema, the_dict)
            break

    return new_schema


def get_dict_from_reference(the_dict: Dict, path: str):
    """Resolves a reference indicated by the `$ref` key into a dict"""

    if path[0:2] != '#/':
        raise MalformedReferenceError(f"The reference {path} must start with '#/'")

    path_elements = path[2:].split('/')

    if path_elements[0] in the_dict:
        resulting_dict = the_dict[path_elements[0]]
    else:
        raise InvalidReferenceError(f"The reference {path} doesn't point to a valid location inside this document")

    for element in path_elements[1:]:
        if element in resulting_dict:
            resulting_dict = resulting_dict[element]
        else:
            raise InvalidReferenceError(f"The reference {path} doesn't point to a valid location inside this document")

    return resulting_dict