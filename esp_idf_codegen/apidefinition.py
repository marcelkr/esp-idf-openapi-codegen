import yaml
from typing import Dict
from datetime import datetime


class ApiInfo:
    def __init__(self, info_dict: Dict):
        self.title = info_dict['title']
        self.version = info_dict['version']
        self.generated_at = datetime.now().isoformat(' ')

    def to_dict(self):
        return {
            'api_title': self.title,
            'api_version': self.version,
            'api_generated_at': self.generated_at
        }


class ApiDefinition:
    __instance = None

    @staticmethod
    def get_instance():
        """ Static access method. """
        if ApiDefinition.__instance is None:
            ApiDefinition()
        return ApiDefinition.__instance

    def __init__(self):
        """ Virtually private constructor. """
        self.api_definition = None
        self.api_info = None
        if ApiDefinition.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            ApiDefinition.__instance = self

    def set_api(self, apifilepath: str) -> None:
        api_file = open(apifilepath)
        self.api_definition = yaml.load(api_file, Loader=yaml.SafeLoader)
        self.api_info: ApiInfo = ApiInfo(self.api_definition['info'])

    def get_api(self) -> Dict:
        return self.api_definition

    def get_info(self) -> Dict:
        return self.api_info