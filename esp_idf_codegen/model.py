import inflection
from typing import Dict
from esp_idf_codegen.cjsonresolver import *
from esp_idf_codegen.exceptions import *
from esp_idf_codegen.apidefinition import ApiDefinition
from esp_idf_codegen.utils import *


class BaseProperty:
    def __init__(self, name: str, property_description: Dict, is_required: bool):
        self.name: str = name
        self.description: str = property_description['description'] if 'description' in property_description else None
        self.is_required: bool = is_required


class Property(BaseProperty):
    def __init__(self, name: str, property_description: Dict, is_required: bool):
        super().__init__(name, property_description, is_required)
        self.datatype: str = property_description['type'] if 'type' in property_description else None
        self.dataformat: str = property_description['format'] if 'format' in property_description else None
        self.min: int = property_description['minimum'] if 'minimum' in property_description else None
        self.max: int = property_description['maximum'] if 'maximum' in property_description else None

        for key in property_description.keys():
            if key == '$ref':
                raise UnsupportedApiSpecificationError('Using $ref in a property is not supported')

        if (self.datatype == 'integer' or self.datatype == 'number') and self.dataformat is None:
            raise MissingPropertyError(
                f'Property {name} with type {self.datatype} needs a valid format. Given format: {self.dataformat}')

    def __repr__(self):
        return f'Property({self.name})'

    def __eq__(self, other) -> bool:
        if self.name == other.name \
                and self.description == other.description \
                and self.datatype == other.datatype \
                and self.dataformat == other.dataformat \
                and self.min == other.min \
                and self.max == other.max \
                and self.is_required == other.is_required:
            return True

        return False

    def to_dict(self):
        resolver = CjsonResolver(self.datatype, self.dataformat)
        struct_element = resolver.get_cjson_struct_element()
        min_check = max_check = None

        if self.min is not None:
            if self.datatype == 'string':
                min_check = f'strlen({self.name}->{struct_element}) < {self.min}'
            else:
                min_check = f'{self.name}->{struct_element} < {self.min}'

        if self.max is not None:
            if self.datatype == 'string':
                max_check = f'strlen({self.name}->{struct_element}) > {self.max}'
            else:
                max_check = f'{self.name}->{struct_element} > {self.max}'

        return {
            'type': self.datatype,
            'c_type': resolver.get_c_type(),
            'struct_element': struct_element,
            'cjson_check_function': resolver.get_cjson_validator_function(),
            'nullvalue': resolver.get_nullvalue(),
            'property_name': self.name,
            'description': self.description,
            'is_required': self.is_required,
            'min?': self.min is not None,
            'min': self.min,
            'min_check': min_check,
            'max?': self.max is not None,
            'max': self.max,
            'max_check': max_check,
            'is_float?': self.datatype == 'number',
            'is_float': 'true' if self.datatype == 'number' else 'false',
        }


class ArrayProperty(BaseProperty):
    def __init__(self, name: str, property_description: Dict, is_required: bool):
        super().__init__(name, property_description, is_required)

        self.datatype: str = property_description['items']['type'] if 'type' in property_description['items'] else None
        self.dataformat: str = property_description['items']['format'] if 'format' in property_description[
            'items'] else None
        self.min_items: int = property_description['minItems'] if 'minItems' in property_description else None
        self.max_items: int = property_description['maxItems'] if 'maxItems' in property_description else None

    def to_dict(self):
        return {
            'type': self.datatype,
            'c_type': 'cJSON*',
            'nullvalue': 'NULL',
            'property_name': self.name,
            'description': self.description,
            'is_required': self.is_required,
            'min_items?': self.min_items is not None,
            'min_items': self.min_items,
            'max_items?': self.max_items is not None,
            'max_items': self.max_items
        }


class BaseModel:
    """The base class for the models to inherit from."""

    @staticmethod
    def factory(name: str, schema: Dict):
        """Based on the given schema creates a `Model` or a `ModelReference` instance"""
        if '$ref' in schema:
            return ModelReference(schema['$ref'])

        return Model(name, schema)


class ModelReference(BaseModel):
    """Represents a reference to a model found in the /components/schemas section of the API spec"""

    def __init__(self, path: str):
        self.path = path


class Model(BaseModel):
    def __init__(self, name: str, schema: Dict):
        self.name: str = name
        self.basename = 'model_' + inflection.underscore(self.name)
        self.typename = self.basename + '_t'
        self.header_filename = self.basename + '.h'
        self.properties: List[BaseProperty] = []
        self.is_used = False

        required_props = []
        if 'required' in schema:
            for prop_name in schema['required']:
                required_props.append(prop_name)

        if '$ref' in schema:
            raise Exception('foo')
        elif 'type' in schema:
            if schema['type'] != 'object':
                raise UnsupportedApiSpecificationError('Type must be object or a refence, not ' + schema['type'])  # FIXME: research: What is allowed as per spec?
            if not schema['properties']:
                raise MissingPropertyError('Properties are empty')

            for property_name, property_schema in schema['properties'].items():
                if property_schema['type'] == 'array':
                    self.properties.append(ArrayProperty(property_name, property_schema, property_name in required_props))
                else:
                    self.properties.append(Property(property_name, property_schema, property_name in required_props))
            # elif schema['type'] == 'string':
            #     self.properties.append(Property(property_name, property_schema, property_name in required_props))
            # elif schema['type'] == 'array':
            #     # TODO: implement
            #     # self.properties.append(ArrayProperty('TODO', schema, 'TODO' in required_props))
            #     raise UnsupportedApiSpecificationError('Array')
                
        elif any(key in schema for key in ['oneOf', 'allOf', 'anyOf']):
            # In case of 'allOf', 'anyOf', 'oneOf' we append all properties to this model
            if 'allOf' in schema:
                subschema = schema['allOf']
            elif 'oneOf' in schema:
                subschema = schema['oneOf']
            else:
                subschema = schema['anyOf']

            for entry in subschema:
                if '$ref' in entry:
                    api = ApiDefinition.get_instance()
                    schema = get_dict_from_reference(api.get_api(), entry['$ref'])
                    for property_name, property_schema in schema['properties'].items():
                        if property_schema['type'] == 'array':
                            self.properties.append(ArrayProperty(property_name, property_schema, property_name in required_props))
                        else:
                            self.properties.append(Property(property_name, property_schema, property_name in required_props))
                elif entry['type'] == 'array':
                    self.properties.append(ArrayProperty(property_name, entry, property_name in required_props))
                elif entry['type'] == 'object':
                    for property_name, property_schema in entry['properties'].items():
                        self.properties.append(Property(property_name, property_schema, property_name in required_props))
                else:
                    raise Exception('foo')
        else:
            raise Exception('foo')  # FIXME: research: What is allowed as per spec?

    def __eq__(self, other) -> bool:
        """A model is considered equal if all properties are equal.

        The model `name`, `basename`, ... are not regarded in this check.
        This is intended and enables filtering duplicate models.
        """
        only_in_self = [x for x in self.properties if x not in other.properties]
        if len(only_in_self) != 0:
            return False

        only_in_other = [x for x in other.properties if x not in self.properties]
        if len(only_in_other) != 0:
            return False

        return True

    def rename(self, new_name: str):
        self.name: str = new_name
        self.basename = 'model_' + inflection.underscore(self.name)
        self.typename = self.basename + '_t'
        self.header_filename = self.basename + '.h'

    def to_dict(self) -> Dict:
        props = [prop.to_dict() for prop in self.properties if isinstance(prop, Property)]
        arrayprops = [prop.to_dict() for prop in self.properties if isinstance(prop, ArrayProperty)]
        return {
            'header_filename': self.header_filename,
            'model_name': self.basename,
            'model_type_name': self.typename,
            'properties': props,
            'arrayproperties': arrayprops,
            'arrayproperties?': len(arrayprops) != 0,
        }