**This is a work in progress. There's still limitations and maybe bugs!**

# ESP-IDF OpenAPI Codegen

This script generates API endpoints for [ESP-IDF](https://github.com/espressif/esp-idf)
projects from [OpenAPI](https://en.wikipedia.org/wiki/OpenAPI_Specification) specifications.
The resulting set of C and H files can directly be used as a component in your
ESP-IDF project. JSON content is automatically parsed and validated using the 
lightweight [cJSON](https://github.com/DaveGamble/cJSON) library.

See the `example` folder which contains an API specification and generated code based on this
speicification.

Currently there's some limitations due to the HTTP stack in the ESP-IDF but also
due to me tailoring the tool to my requirements. See the [Limitations](#limitations)
section.

## Features and Details

- Verifies the presence and type of JSON properties in the request body.
- Checks minimum and maximum of parameters as defined in the API specification.
- Generates callbacks in separate files with convenient structs representing your API's models.
- Groups the routes logically (e.g. `/factories` and `/factory/employees` will end up in `api_factory.c`).
- Uses the lightweight [cJSON](https://github.com/DaveGamble/cJSON) to parse JSON

## Limitations

- One can only use `$ref` as a child of `schema:`.
- Path parameters are not supported (e.g.: `/users/{user_id}`) due to ESP-IDF limitations 
  (and me being to lazy to go deeper into the ESP HTTP stack than needed for now).
- See the tickets of this repo for more nice-to-have things.

## Usage

It's probably best if you have a look at the `example` folder to understand how a generated API 
looks like. If you want to start from scratch, read on.

### API generation

For code generation you only need a valid OpenAPI YAML specification. Just call the
script with the file as argument like so: `main.py my_api_spec.yaml` and you should
end up with a `generated/` folder including all the c sources and headers.

There's various switches to change the behaviour of the generator. You might want to play 
around with:

- `-o` which let's you define a folder to generate the files to.
- `-f` which overwrites the generated files. `-ff` overwrites also the
  files containing the user-editable callbacks.

For more details run `main.py --help`.

### ESP-IDF integration

#### 1. Generate the API component

Using the `-o` option you can directly generate into your ESP-IDF project's component folder.
E.g. you can do something like this:

    main.py my_api_spec.yaml -o ~/my-esp-project/components/api/

If you want to update the files you can use the `-f` or `-ff` switch to overwrite existing files. 
Be aware that modifications to the generated files will be overwritten.

#### 2. Add a CMakeLists.txt file

Add a `CMakeLists.txt` file to the component and all all source files. 
Something like this will do:

    idf_component_register(
        SRCS
            api.c
            api_foo.c
            api_bar.c
        REQUIRES
            esp_http_server
            cjson
        INCLUDE_DIRS
            "."
    )

#### 3. Add cJSON as a component

The generated API stubs rely on [cJSON](https://github.com/DaveGamble/cJSON) for JSON handling.
Add cJSON as a component by following the steps below.

1. Add a `components/cjson` directory to your ESP-IDF project.
2. Clone [cJSON](https://github.com/DaveGamble/cJSON) into `components/cjson/cJSON`. If you're
   inside a git project already consider adding [cJSON as a submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules).
3. Add a `components/cjson/CMakeLists.txt` for cJSON with the following content:
    ```
    idf_component_register(
        SRCS "cJSON/cJSON.c"
        INCLUDE_DIRS "cJSON"
    )
    ```