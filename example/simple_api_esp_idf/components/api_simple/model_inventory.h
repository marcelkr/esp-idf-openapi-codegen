// This file is auto-generated. DO NOT EDIT!
//
// This file was generated at 2021-06-22 18:52:48.127963.
// It is part of the API 'Simple Inventory API' version 1.0.0.

#pragma once

#include <stdint.h>
#include <stdbool.h>

typedef struct {
    
    int8_t id;
    
    char* name;
    
    char* releaseDate;
} model_inventory_t;
