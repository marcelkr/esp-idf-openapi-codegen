// This file is auto-generated but for you to edit.
//
// This file was generated at 2021-06-22 08:43:23.746533.
// It is part of the API Simple Inventory API version 1.0.0.

#include "api_inventory_user.h"
#include "model_inventory.h"
#include "api_utils.h"
#include "esp_http_server.h"
#include "esp_log.h"

void inventory_get_user(response_data_t* response_data)
{
    // TODO: Implement your logic here to fill 'response_data'.
    // As per API spec you must implement the following responses:
    //  - 200: search results matching criteria
    //  - 400: bad input parameter

    ESP_LOGI("API_CBK", "Hello from inventory_get_user()");
}

void inventory_post_user(response_data_t* response_data, const model_inventory_t* model_inventory)
{
    // TODO: Implement your logic here to fill 'response_data'.
    // As per API spec you must implement the following responses:
    //  - 201: item created
    //  - 400: invalid input, object invalid
    //  - 409: an existing item already exists

    ESP_LOGI("API_CBK", "Hello from inventory_post_user()");
}

