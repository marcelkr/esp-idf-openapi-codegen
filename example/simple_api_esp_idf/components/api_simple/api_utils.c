// This file is auto-generated. DO NOT EDIT!
//
// This file was generated at 2021-06-22 18:52:48.127963.
// It is part of the API 'Simple Inventory API' version 1.0.0.

#include "api_utils.h"
#include "cJSON.h"
#include "esp_err.h"
#include "esp_log.h"
#include "esp_http_server.h"


esp_err_t handle_json_body(httpd_req_t *req, char* content, const size_t content_len, cJSON* json) {
    size_t recv_size = 0;

    // Make sure not to exceed the receive buffer
    if (req->content_len > content_len) {
        recv_size = content_len;
        ESP_LOGW(TAG, "HTTP body recv buffer truncated. Content len: %d, buffer len: %d", req->content_len, content_len);
    } else {
        recv_size = req->content_len;
    }

    int ret = httpd_req_recv(req, content, recv_size);
    if (ret <= 0) {
        if (ret == HTTPD_SOCK_ERR_TIMEOUT) {
            /* In case of timeout one can choose to retry calling
             * httpd_req_recv(), but to keep it simple, here we
             * respond with an HTTP 408 (Request Timeout) error */
            httpd_resp_send_408(req);
        } else {
            httpd_resp_send_500(req);
        }

        // An error *must* be returned if httpd_req_recv fails to cleanup the socket
        return ESP_FAIL;
    }

    // Parse the bodys content
    json = cJSON_Parse(content);
    if (json == NULL) {
        // JSON error handling stuff
        // const char *error_ptr = cJSON_GetErrorPtr();
        // if (error_ptr != NULL) {
        //     // ESP_LOGE("FERTILIZER_HTTPD", "Error before: %s\n", error_ptr);
        // } else {
        //     // ESP_LOGE("FERTILIZER_HTTPD", "error_ptr");
        // }
        cJSON_Delete(json);
        httpd_resp_send_500(req);
    }

    return ESP_OK;
}

void handle_json_missing_property_error(httpd_req_t *req, const char* param_name)
{
    char msg[100];
    snprintf(msg, sizeof(msg), "{\"message\": \"Required property %s is missing\"}", param_name);
    httpd_resp_set_status(req, "400 Bad Request");
    httpd_resp_set_type(req, HTTPD_TYPE_JSON);
    httpd_resp_sendstr(req, msg);
}

void handle_json_validation_error(httpd_req_t *req, const char* param_name, const char* datatype_name)
{
    char msg[100];
    snprintf(msg, sizeof(msg), "{\"message\": \"Property %s is not a %s\"}", param_name, datatype_name);
    httpd_resp_set_status(req, "400 Bad Request");
    httpd_resp_set_type(req, HTTPD_TYPE_JSON);
    httpd_resp_sendstr(req, msg);
}

void handle_range_error(httpd_req_t *req, const char* param_name, const range_error_data_t* err_data)
{
    char msg[100];
    char indication[21];

    if (err_data->error_type == OVER_MAXIMUM) {
        strcpy(indication, "exceeds the maximum");
    } else {
        strcpy(indication, "is below the minimum");
    }

    if (err_data->is_float == true) {
        const char* json = "{\"message\": \"The value of %s %s allowed value of %f\"}";
        snprintf(msg, sizeof(msg), json, param_name, indication, err_data->float_limit);
    } else {
        const char* json = "{\"message\": \"The value of %s %s allowed value of %llu\"}";
        snprintf(msg, sizeof(msg), json, param_name, indication, err_data->int_limit);
    }

    httpd_resp_set_status(req, "400 Bad Request");
    httpd_resp_set_type(req, HTTPD_TYPE_JSON);
    httpd_resp_sendstr(req, msg);
}

const char* get_error_status(const httpd_err_code_t error)
{
    const char *status;

    switch (error) {
        case HTTPD_501_METHOD_NOT_IMPLEMENTED:
            status = "501 Method Not Implemented";
            break;
        case HTTPD_505_VERSION_NOT_SUPPORTED:
            status = "505 Version Not Supported";
            break;
        case HTTPD_400_BAD_REQUEST:
            status = "400 Bad Request";
            break;
        case HTTPD_404_NOT_FOUND:
            status = "404 Not Found";
            break;
        case HTTPD_405_METHOD_NOT_ALLOWED:
            status = "405 Method Not Allowed";
            break;
        case HTTPD_408_REQ_TIMEOUT:
            status = "408 Request Timeout";
            break;
        case HTTPD_414_URI_TOO_LONG:
            status = "414 URI Too Long";
            break;
        case HTTPD_411_LENGTH_REQUIRED:
            status = "411 Length Required";
            break;
        case HTTPD_431_REQ_HDR_FIELDS_TOO_LARGE:
            status = "431 Request Header Fields Too Large";
            break;
        case HTTPD_500_INTERNAL_SERVER_ERROR:
        default:
            status = "500 Internal Server Error";
    }

    return status;
}
