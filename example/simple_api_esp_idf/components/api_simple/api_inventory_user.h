// This file is auto-generated but for you to edit.
//
// This file was generated at 2021-06-22 08:43:23.746533.
// It is part of the API Simple Inventory API version 1.0.0.

#pragma once

#include "model_inventory.h"
#include "api_utils.h"

void inventory_get_user(response_data_t* response_data);
void inventory_post_user(response_data_t* response_data, const model_inventory_t* model_inventory);
