// This file is auto-generated. DO NOT EDIT!
//
// This file was generated at 2021-06-22 18:52:48.127963.
// It is part of the API 'Simple Inventory API' version 1.0.0.

#pragma once

#include "esp_err.h"
#include "esp_http_server.h"

void api_inventory_init(httpd_handle_t httpd_handle);
