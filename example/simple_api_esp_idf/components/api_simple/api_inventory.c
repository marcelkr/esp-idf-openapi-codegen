// This file is auto-generated. DO NOT EDIT!
//
// This file was generated at 2021-06-22 18:52:48.127963.
// It is part of the API 'Simple Inventory API' version 1.0.0.

#include "api_inventory.h"
#include "model_inventory.h"
#include "api_inventory_user.h"
#include "api_utils.h"
#include "cJSON.h"
#include "esp_err.h"
#include "esp_log.h"
#include "esp_http_server.h"
#include <stdbool.h>

static const char* TAG = "API";

/**
 * @brief GET handler for URL /inventory.
 *
 * searches inventory
 *
 * **Responses**
 * The user must implement the following responses in the callback
 * function inventory_get_user(...)
 *  - 200: search results matching criteria
 *  - 400: bad input parameter
*/
static esp_err_t inventory_get(httpd_req_t *req)
{
    // Query string handling
    char* buf;
    size_t buf_len;
    buf_len = httpd_req_get_url_query_len(req) + 1;
    if (buf_len > 1) {
        buf = malloc(buf_len);
        if (httpd_req_get_url_query_str(req, buf, buf_len) == ESP_OK) {
            ESP_LOGI(TAG, "Found URL query => %s", buf);
            char param[32];

            if (httpd_query_key_value(buf, "searchString", param, sizeof(param)) == ESP_OK) {
                ESP_LOGI(TAG, "Found URL query parameter => query1=%s", param);
            }
            if (httpd_query_key_value(buf, "skip", param, sizeof(param)) == ESP_OK) {
                ESP_LOGI(TAG, "Found URL query parameter => query1=%s", param);
            }
            if (httpd_query_key_value(buf, "limit", param, sizeof(param)) == ESP_OK) {
                ESP_LOGI(TAG, "Found URL query parameter => query1=%s", param);
            }
        }
        free(buf);
    }

    // Prepare response data struct for user callback function
    response_data_t response_data = {
        .content = NULL,
        .content_len = 0,
        .has_error = false,
        .error_code = HTTPD_500_INTERNAL_SERVER_ERROR,
    };

    inventory_get_user(&response_data);

    if (response_data.has_error) {
        httpd_resp_set_status(req, get_error_status(response_data.error_code));
    }

    httpd_resp_set_type(req, HTTPD_TYPE_JSON);
    httpd_resp_sendstr(req, response_data.content);

    free(response_data.content);

    return ESP_OK;
}

static const httpd_uri_t uri_inventory_get = {
    .uri      = "/inventory",
    .method   = HTTP_GET,
    .handler  = inventory_get,
    .user_ctx = NULL
};

/**
 * @brief POST handler for URL /inventory.
 *
 * adds an inventory item
 *
 * **Request body**
 * The request body contains the following parameters:
 *  - id (int8_t, required)
 *  - name (char*, required)
 *  - releaseDate (char*, required)
 *
 * **Responses**
 * The user must implement the following responses in the callback
 * function inventory_post_user(...)
 *  - 201: item created
 *  - 400: invalid input, object invalid
 *  - 409: an existing item already exists
*/
static esp_err_t inventory_post(httpd_req_t *req)
{
    // Body handling
    char content[100];
    cJSON* json = NULL;

    esp_err_t err = handle_json_body(req, content, sizeof(content), json);

    if (err != ESP_OK) {
        return err;
    }

    // Validate property id
    cJSON *id = cJSON_GetObjectItemCaseSensitive(json, "id");
    if (id == NULL) {
        handle_json_missing_property_error(req, "id");
        cJSON_Delete(json);
        return ESP_OK;
    }
    if (!cJSON_IsNumber(id)) {
        handle_json_validation_error(req, "id", "integer");
        cJSON_Delete(json);
        return ESP_OK;
    }


    // Validate property name
    cJSON *name = cJSON_GetObjectItemCaseSensitive(json, "name");
    if (name == NULL) {
        handle_json_missing_property_error(req, "name");
        cJSON_Delete(json);
        return ESP_OK;
    }
    if (!cJSON_IsString(name)) {
        handle_json_validation_error(req, "name", "string");
        cJSON_Delete(json);
        return ESP_OK;
    }


    // Validate property releaseDate
    cJSON *releaseDate = cJSON_GetObjectItemCaseSensitive(json, "releaseDate");
    if (releaseDate == NULL) {
        handle_json_missing_property_error(req, "releaseDate");
        cJSON_Delete(json);
        return ESP_OK;
    }
    if (!cJSON_IsString(releaseDate)) {
        handle_json_validation_error(req, "releaseDate", "string");
        cJSON_Delete(json);
        return ESP_OK;
    }


    // Prepare model data struct for user callback function
    model_inventory_t model_inventory = {
        
        .id = id == NULL ? 0 : id->valueint,
        
        .name = name == NULL ? NULL : name->valuestring,
        
        .releaseDate = releaseDate == NULL ? NULL : releaseDate->valuestring,
    };

    // Prepare response data struct for user callback function
    response_data_t response_data = {
        .content = NULL,
        .content_len = 0,
        .has_error = false,
        .error_code = HTTPD_500_INTERNAL_SERVER_ERROR,
    };

    inventory_post_user(&response_data, &model_inventory);

    if (response_data.has_error) {
        httpd_resp_set_status(req, get_error_status(response_data.error_code));
    }

    httpd_resp_set_type(req, HTTPD_TYPE_JSON);
    httpd_resp_sendstr(req, response_data.content);

    free(response_data.content);

    return ESP_OK;
}

static const httpd_uri_t uri_inventory_post = {
    .uri      = "/inventory",
    .method   = HTTP_POST,
    .handler  = inventory_post,
    .user_ctx = NULL
};


void api_inventory_init(httpd_handle_t httpd_handle) {
    if (NULL == httpd_handle) {
        ESP_LOGE(TAG, "HTTPD handle is NULL. Couldnt register URI.");
    } else {
        httpd_register_uri_handler(httpd_handle, &uri_inventory_get);
        httpd_register_uri_handler(httpd_handle, &uri_inventory_post);
    }
}
