// This file is auto-generated. DO NOT EDIT!
//
// This file was generated at 2021-06-22 18:52:48.127963.
// It is part of the API 'Simple Inventory API' version 1.0.0.

#include "api.h"
#include "api_inventory.h"

#include "esp_log.h"
#include "esp_err.h"
#include "esp_http_server.h"

static const char* TAG = "API";

void api_init(httpd_handle_t httpd_handle) {
    if (NULL == httpd_handle) {
        ESP_LOGE(TAG, "HTTPD handle is NULL. Couldnt register URI.");
    } else {
        api_inventory_init(httpd_handle);
    }
}
