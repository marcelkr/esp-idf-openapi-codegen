# Simple API example

This example is based on the API which you can create on SwaggerHub using
the template 'Simple API' when creating a new project.

`simple_api.yaml` contains the API specification. Parts which are not yet
supported by the generator have been removed. Namely the `$ref` to the
`Manufacturer` scheme in the components.

For demo purposes I put some content in the functions in `api_inventory_user.c`.
`inventory_get_user()` will give you a 200 status code with a json 'hello world'.
`inventory_post_user()` will give you a 400 with 'hello error'.

**Note:** Query parameters are not yet supported. I'm on it.

**Note:** The `response_data->content` is automatically freed in the calling
function. No need to worry.

## API generation

The generated files are already there. Have a look in `components/api_simple`.
If you want to regenerate them use this command from the main folder:

    python main.py example/simple_api.yaml -f -o example/simple_api_esp_idf_example/components/api_simple/

## Compiling

The software can be compiled like any other ESP IDF project. 
Something like this usually:

- `mkdir build && cd build`
- `cmake -G Ninja ..`
- `ninja menuconfig` - **Important:** Adapt your WiFi credentials in the menu entry
  *Example Configuration*
- `ninja`

Don't forget to activate the ESP IDF environment first.

## Play around

Use CURL or some more sophisticated tool to test the API. In the top console in this screenshot 
the ESP serial monitor is shown. In the bottom my `curl` requests can be seen.

![Terminal screenshot](screenshot_terminal.png)