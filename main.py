import sys
import argparse
from datetime import datetime
from typing import List, Dict, Optional
from pathlib import Path
import inflection
import chevron
import yaml
from esp_idf_codegen.codegen import Codegenerator, ApiDefinition


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('openapispec', type=str, help='The OpenAPI input file in YAML format.')
    parser.add_argument('--only-used-models', dest='used_models_only', action='store_true',
                        help='Only output models which are used instead of all models.')
    parser.add_argument('-o', '--out-dir', dest='out_dir', default='generated', type=str,
                        help='The directory which the generated files will be written to')
    parser.add_argument('-p', '--print-only', dest='print', action='store_true')
    parser.add_argument('-f', '--overwrite', dest='overwrite', action='count', default=0,
                        help='Overwrite existing files. Use -f to overwrite existing files '
                             'except the ones meant to be user-editable. Use -ff to overwrite all files.')
    parser.add_argument('-s', '--settingsfile', dest='settingsfile', default='', type=str,
                        help='A file containing generation settings')
    args = parser.parse_args()

    api = ApiDefinition.get_instance()
    api.set_api(args.openapispec)

    codegen = Codegenerator(args.settingsfile)

    if args.print:
        codegen.print_routes()
        sys.exit(0)

    out_path = Path(args.out_dir)
    if out_path.exists() and not out_path.is_dir():
        print(f'Output path "{out_path}" exists but is not a directory. Exiting.')
        sys.exit(1)
    elif not out_path.exists():
        out_path.mkdir()

    codegen.generate(out_path, args.overwrite, args.used_models_only)


if __name__ == '__main__':
    main()
