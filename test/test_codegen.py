from esp_idf_codegen.codegen import Codegenerator, ApiDefinition

def test_print_simple_api_without_error():
    api = ApiDefinition.get_instance()
    api.set_api('test/simple_api.yaml')
    codegen = Codegenerator()
    codegen.print_routes()

def test_gen_simple_api_without_error(tmp_path):
    api = ApiDefinition.get_instance()
    api.set_api('test/simple_api.yaml')
    codegen = Codegenerator()
    codegen.generate(tmp_path, True, True)