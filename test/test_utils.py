from esp_idf_codegen.utils import *
import pytest

test_dict_noref = {
    'hello': 'world',
    'foo': {
        'bar': 'baz'
    }
}

test_dict_1 = {
    '$ref': '#/foo/bar',
    'foo': {
        'bar': 'baz'
    }
}

test_dict_2 = {
    '$ref': '#/foo/bar',
    'foo': {
        'bar': {'hello': 'world'}
    }
}

test_dict_3 = {
    'foo': {
        'bar': {'$ref': '#/seg1/seg2'}
    },
    '$ref': '#/foo/bar',
    'seg1': {
        'seg2': {'key1': 'val1'}
    }
}

def test_returns_string():
    result = get_dict_from_reference(test_dict_1, '#/foo/bar')
    assert result == 'baz'

def test_returns_dict():
    result = get_dict_from_reference(test_dict_2, '#/foo/bar')
    assert 'hello' in result
    assert result['hello'] == 'world'

def test_exception_on_invalid_path_1():
    with pytest.raises(InvalidReferenceError):
        get_dict_from_reference(test_dict_1, '#/bar')

def test_exception_on_invalid_path_2():
    with pytest.raises(InvalidReferenceError):
        get_dict_from_reference(test_dict_1, '#/foo/asdf')

def test_exception_on_malformed_path_1():
    with pytest.raises(MalformedReferenceError):
        get_dict_from_reference(test_dict_1, '/foo/bar')

def test_exception_on_malformed_path_2():
    with pytest.raises(MalformedReferenceError):
        get_dict_from_reference(test_dict_1, 'foo/bar')

def test_resolves_dict_noref():
    resolved_dict = get_resolved_schema(test_dict_noref, test_dict_noref)

    resolved_dict_compare = {
        'hello': 'world',
        'foo': {
            'bar': 'baz'
        }
    }

    assert resolved_dict == resolved_dict_compare

def test_resolves_dict_simple_ref():
    resolved_dict = get_resolved_schema(test_dict_2, test_dict_2)

    resolved_dict_compare = {
        'hello': 'world',
        'foo': {
            'bar': {'hello': 'world'}
        }
    }

    assert resolved_dict == resolved_dict_compare

def test_resolves_dict_nested_ref():
    resolved_dict = get_resolved_schema(test_dict_3, test_dict_3)

    resolved_dict_compare = {
        'key1': 'val1',
        'foo': {
            'bar': {'key1': 'val1'}
        },
        'seg1': {
            'seg2': {'key1': 'val1'}
        }
    }

    assert resolved_dict == resolved_dict_compare
